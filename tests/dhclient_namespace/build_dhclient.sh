#!/bin/bash
ip netns add h_dhcp
ip link add veth0 type veth peer name eth-dhclient
ip link set veth0 netns h_dhcp
ip netns exec h_dhcp ip link set veth0 up

# we attach the other end to the docker bridge
ip link set eth-dhclient master br-62812fe6e810
ip link set eth-dhclient up

ip netns exec h_dhcp ifconfig

if [ `docker container ps | grep "dnsmasq" | wc -l` = '1' ]
then
	ip netns exec h_dhcp dhclient veth0
	ip netns exec h_dhcp ifconfig
fi;
