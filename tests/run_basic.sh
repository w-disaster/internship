#!/bin/bash

# build dhclient image
docker build -t dhclient ./basic_dhclient/

# create docker subnetwork
docker network create --subnet 137.204.0.0/24 -o com.docker.network.bridge.name=br_lan lan

# we run the dns/dhcp server and we connect it to the created bridge with the specified ip
docker run \
	--privileged \
	--name dnsmasq \
	-d \
	-p 53:53/udp \
	-p 5380:8080 \
	-v $PWD/dnsmasq.conf:/etc/dnsmasq.conf \
	--log-opt "max-size=100m" \
	-e "HTTP_USER=foo" \
	-e "HTTP_PASS=bar" \
	--restart always \
	--network lan --ip 137.204.0.30 \
	jpillora/dnsmasq

# we run the dhclient connecting it to the same server bridge
docker run \
	--privileged \
	--name dhclient \
	-d \
	--network lan \
	dhclient

# we delete the default ip given by docker
docker exec dhclient ip addr flush dev eth0
