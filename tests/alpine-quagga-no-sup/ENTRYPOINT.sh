#!/bin/ash

echo "Starting zebra daemon..."
/usr/sbin/zebra -d -f /etc/quagga/zebra.conf

echo "Starting rip deamon ..."
/usr/sbin/ripd -d -f /etc/quagga/ripd.conf

vtysh
