#!/bin/bash
# Build images
echo "----------------- IMAGES ----------------"
docker build -t dnsmasq ./images/dnsmasq/
docker build -t iodined ./images/iodine_server/
docker build -t iodine ./images/iodine_client/

# Docker network
echo "---------------- NETWORK ----------------"
docker network create --subnet 137.204.0.0/24 -o com.docker.network.bridge.name=br_lan lan

# We run the dns/dhcp server and we connect it to the created bridge with the specified IP
# See dnsmasq.conf for the configuration
echo "---------------- DNSMASQ ----------------"
docker run -d \
        --privileged \
        --name dnsmasq \
        -v $PWD/conf/dnsmasq/dnsmasq.conf:/etc/dnsmasq.conf \
        --network lan --ip 137.204.0.30 \
        dnsmasq

# Iodine server container run with static IP address
echo "------------- IODINE SERVER -------------"
docker run -d -it --privileged --name iodined --network lan --ip 137.204.0.40 iodined
# Iodine server side: answer all iodine.mydomain queries
docker exec iodined /opt/iodine/bin/iodined -c -P password -4 10.0.0.1 iodine.mydomain

# Iodine client container run with static IP address
echo "------------- IODINE CLIENT -------------"
docker run -d -it --privileged --name iodine --network lan --ip 137.204.0.41 iodine
# Iodine client side: redirect all queries to dnsmasq (DNS) asking for iodine.mydomain domain
docker exec iodine /opt/iodine/bin/iodine -P password -4 137.204.0.30 iodine.mydomain

# We estabilish encripted connection through DNS tunnel. Password: "root"
docker exec -it iodine ssh root@10.0.0.1
