#!/bin/bash
# Build images: Dnsmasq, Iodine server and client, DHCP client
docker-compose -f ./docker-compose.yml up -d 

# Run commands for the IPv4 tunnel
echo "------------- IODINE SERVER -------------"
docker exec iodined /opt/iodine/bin/iodined -c -P password -4 10.0.0.1 iodine.mydomain
echo "------------- IODINE CLIENT -------------"
docker exec iodine /opt/iodine/bin/iodine -P password -4 137.204.0.30 iodine.mydomain

# We estabilish encripted connection through DNS tunnel. Password: "root"
echo "--------- SSH to IODINE SERVER -----------"
docker exec -it iodine ssh root@10.0.0.1
