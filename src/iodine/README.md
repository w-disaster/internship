## IODINE DNS tunnel

Run ``` bash run_iodine_containers.sh ``` to build the topology.
The script initially creates the images and the docker network, then it runs:

* dnsmasq container: DNS server to resolve iodine.mydomain: the iodine server
* iodine server: the iodined daemon creates a virtual IP to tunnel data from iodine.mydomain queries
* iodine client: iodine daemon searches for iodine.mydomain, redirecting queries to dnsmasq. Once resolved it creates a virtual IP. Finally SSH to the server virtual IP and the DNS tunnel is set up.
