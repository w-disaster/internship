## DHCLIENT container

To run the DHCP client we introduce you two different scripts.
For both modes we need a DHCP server container that dyanamically assigns the IPs: in our study case is dnsmasq,
a lightweight DNS/DHCP server. dnsmasq accepts a configuration file where we can specify the IP address range to assign to hosts,
their lease time, the domains to resolve and more.
 
* ``` bash run_dhclient_wnamespace.sh ``` : running this script we initially create the images and the docker network. 
Next, we launch a container running dnsmasq, mounting the configuration file on the required path and connecting it to the docker network with a static IP. For the DHCP client, we first run an Alpine Linux container not connected to any networks. Then, extracting the PID of the container, we link its network namespace to the host's, allowing us to attach an interface to the virtual bridge of the docker network. Lastly, we run dhclient to the created interface to dynamically assign its IP.

* ``` bash run_dhclient_wplugin.sh ``` : running this script, we create two docker networks attached to the same brigde. One is a docker network that uses the default network driver (bridge); the other uses a custom one that discovers an IP for every attached container, assigning it automatically, if found. This driver achieves this by sending DHCP DISCOVER on the same bridge.
This topology needs two docker networks because the one with the custom driver does not allow us to assign static IP to the containers and in order to run dnsmasq to provide the IPs we need both.
