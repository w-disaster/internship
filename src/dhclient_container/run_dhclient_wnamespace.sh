#!/bin/bash
# Build dnsmasq image
echo "------------- DNSMASQ IMAGE -------------"
docker build -t dnsmasq ./images/dnsmasq/
# Docker network
echo "---------------- NETWORK ----------------"
docker network create --subnet 137.204.0.0/24 -o com.docker.network.bridge.name=br_lan lan

# We run the dns/dhcp server and we connect it to the created bridge with the specified IP
# See dnsmasq.conf for the configuration
echo "---------------- DNSMASQ ----------------"
docker run -d \
        --privileged \
        --name dnsmasq \
        -v $PWD/conf/dnsmasq/dnsmasq.conf:/etc/dnsmasq.conf \
        --network lan --ip 137.204.0.30 \
        dnsmasq

# Run basic Alpine linux container without outwards connections
echo "-------------- DHCP CLIENT --------------"
docker run -it -d --name dhclient-cont --network=none alpine

# Set visible the container namespace
mkdir -p /var/run/netns
pid="$(docker inspect dhclient-cont -f '{{.State.Pid}}')"
ln -sf /proc/$pid/ns/net /var/run/netns/dhclient-cont

# Create the interface, one end attached to the bridge, the other at container namespace
ip link add veth0 type veth peer name eth-dhc
ip link set veth0 netns dhclient-cont
ip netns exec dhclient-cont ip link set veth0 up

# Here, br_lan is the bridge created by the Docker network "lan"
ip link set eth-dhc master br_lan
ip link set eth-dhc up

# We launch dhclient to the container end
ip netns exec dhclient-cont dhclient veth0

# IP check
docker exec dhclient-cont ifconfig
