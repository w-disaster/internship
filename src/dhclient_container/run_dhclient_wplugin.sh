#!/bin/bash

# Build dnsmasq image
echo "------------- DNSMASQ IMAGE -------------"
docker build -t dnsmasq ./images/dnsmasq/

# We create the lan to connect dhclient and dns/dhcp server
ip link add br_lan type bridge
ip link set br_lan up

# This docker bridge driver has the purpose to configure each connected
# container's IP using DHCP through a DHCP server also connected to the bridge.
# https://github.com/devplayer0/docker-net-dhcp
echo "---------------- PLUGIN ----------------"
docker plugin install devplayer0/net-dhcp

# The network with the installed driver
# The null IPAM driver must be used, or else Docker will try to allocate IP addresses from its choice of subnets
echo "---------------- NETWORKS ----------------"
docker network create -d devplayer0/net-dhcp:latest --ipam-driver null -o bridge=br_lan dhcp_net

# Create docker subnetwork for dns/dhcp server
docker network create --subnet 137.204.0.0/24 -o com.docker.network.bridge.name=br_lan lan

# We run the dns/dhcp server and we connect it to the created bridge with the specified IP
# See dnsmasq.conf for the configuration
echo "---------------- DNSMASQ ----------------"
docker run -d \
        --privileged \
        --name dnsmasq \
        -v $PWD/conf/dnsmasq/dnsmasq.conf:/etc/dnsmasq.conf \
        --network lan --ip 137.204.0.30 \
        dnsmasq

# Run basic Alpine linux container, assigning to it a dynamic IP
echo "-------------- DHCP CLIENT --------------"
docker run -it -d --name dhclient-cont --network dhcp_net alpine

# IP check
docker exec dhclient-cont ifconfig
