## VPN IPsec strongSwan

Three topologies: Net-to-Net, Road Warrior with NAT-Traversal, Host-to-Host.

Both hosts and VPN endpoints are Alpine Linux containers. To run the VPN endpoints, only the swanctl.conf file must be mounted on the container because the certificates are added by the Dockefile e.g. when the images are built (see the images directory's files) and strongSwan connections are automatically set up at container start.
For the first two topologies hosts located behind the VPN endpoints, we need to change their default route in order to reach the destinations.

The Host-to-Host topology is for exercising purpose and the directory only contains the configuration files and a script to build the images. The certificates are even here automatically added but the charon IKE daemon, strongSwan certificates and connections must be loaded by who want to test.
