mkdir -p /var/run/netns
pid="$(docker inspect h_rw -f '{{.State.Pid}}')"
ln -sf /proc/$pid/ns/net /var/run/netns/h_rw

ip link add veth0 type veth peer name eth-h-rw
ip link set veth0 netns h_rw
ip netns exec h_rw ip link set veth0 up
ip link set eth-h-rw master br_pr
ip link set eth-h-rw up
ip netns exec h_rw ip addr add 192.168.1.10/24 dev veth0
ip netns exec h_rw ip route del default
ip netns exec h_rw ip route add default via 192.168.1.254


