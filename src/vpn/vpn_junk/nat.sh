#docker network create --subnet 192.168.1.0/24 -o com.docker.network.bridge.name=br_pr private_net
docker run -it -d --privileged --name nat --network pr_lan --ip 192.168.1.254 alpine
docker exec -it nat apk add iptables

docker network connect --ip 137.204.0.2 lan nat
docker exec -it nat ip route del default
docker exec -it nat ip route add default via 137.204.0.3

#mkdir -p /var/run/netns
#pid="$(docker inspect nat -f '{{.State.Pid}}')"
#ln -sf /proc/$pid/ns/net /var/run/netns/nat

#ip link add veth0 type veth peer name eth-nat
#ip link set veth0 netns nat
#ip netns exec nat ip link set veth0 up
#ip link set eth-nat master lan
#ip link set eth-nat up
#ip netns exec nat ip addr ad 137.204.0.2/24 dev veth0
#ip netns exec nat ip route del default
#ip netns exec nat ip route add default via 137.204.0.3

# regole
# iptables -t nat -A PREROUTING -i eth1 -j DNAT --to 192.168.1.10
# iptables -t nat -A POSTROUTING -o veth0 -j MASQUERADE
