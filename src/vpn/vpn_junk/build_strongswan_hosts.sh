# host on LAN behind moon
ip netns add h1_moon
ip link add veth0 type veth peer name eth-h1_moon
ip link set veth0 netns h1_moon
ip netns exec h1_moon ip link set veth0 up
ip link set eth-h1_moon master br_moon_lan
ip link set eth-h1_moon up
ip netns exec h1_moon ip addr add 10.1.0.10/24 dev veth0
ip netns exec h1_moon ip route add default via 10.1.0.254


# host on LAN behind sun
ip netns add h1_sun
ip link add veth0 type veth peer name eth-h1_sun
ip link set veth0 netns h1_sun
ip netns exec h1_sun ip link set veth0 up
ip link set eth-h1_sun master br_sun_lan
ip link set eth-h1_sun up
ip netns exec h1_sun ip addr add 10.2.0.10/24 dev veth0
ip netns exec h1_sun ip route add default via 10.2.0.254
