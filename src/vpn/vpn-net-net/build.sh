#!/bin/bash
# images build
echo "----------- DOCKER IMAGES -----------"
docker build -t moon ./images/moon/
docker build -t sun ./images/sun/

# networks
echo "------------- NETWORKS -------------"
docker network create --subnet=137.204.0.0/24 -o com.docker.network.bridge.name=br_lan lan
docker network create --subnet=10.1.0.0/24 -o com.docker.network.bridge.name=br_moon_lan moon_lan
docker network create --subnet=10.2.0.0/24 -o com.docker.network.bridge.name=br_sun_lan sun_lan

# gateway moon
echo "--------- MOON VPN GATEWAY ---------"
docker run -d -it --privileged \
	--name moon \
	-v $PWD/conf/moon/quagga/:/etc/quagga \
	-v $PWD/conf/moon/swanctl.conf:/etc/swanctl/swanctl.conf \
	--network moon_lan --ip 10.1.0.254 \
	 moon
docker network connect --ip 137.204.0.2 lan moon

echo "------------- H1_MOON -------------"
docker run -d -it --privileged \
	--name h1_moon \
	--network moon_lan --ip 10.1.0.10 \
	alpine
docker exec -it h1_moon ip route del default
docker exec -it h1_moon ip route add default via 10.1.0.254

# gateway sun
echo "---------- SUN VPN GATEWAY ---------"
docker run -d -it --privileged \
	--name sun \
	-v $PWD/conf/sun/quagga/:/etc/quagga \
	-v $PWD/conf/sun/swanctl.conf:/etc/swanctl/swanctl.conf \
	--network sun_lan --ip 10.2.0.254 \
	sun
docker network connect --ip 137.204.0.3 lan sun

echo "------------- H1_SUN --------------"
docker run -d -it --privileged \
	--name h1_sun \
	--network sun_lan --ip 10.2.0.10 \
	alpine
docker exec -it h1_sun ip route del default
docker exec -it h1_sun ip route add default via 10.2.0.254

# test
echo "------- H1_MOON --> H1_SUN --------"
docker exec -it h1_moon ping 10.2.0.10

