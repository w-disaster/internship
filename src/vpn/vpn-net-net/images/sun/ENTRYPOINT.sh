#!/bin/sh
/usr/sbin/zebra -f /etc/quagga/zebra.conf &
/usr/sbin/ripd -f /etc/quagga/ripd.conf &
/usr/lib/strongswan/charon & 
sleep 0.5
swanctl --load-creds
swanctl --load-conns
/bin/sh
