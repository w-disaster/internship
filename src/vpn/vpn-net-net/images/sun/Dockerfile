# HEADER
FROM alpine:latest

# Install packages
RUN apk add quagga \
	&& apk add strongswan \
	&& apk add openrc \
	&& apk add busybox-extras 

# Entrypoint script
COPY ENTRYPOINT.sh /etc/ENTRYPOINT.sh
# Configuration files
VOLUME /etc/quagga

# Strongswan config
COPY strongswanCert.pem /etc/swanctl/x509ca/strongswanCert.pem
COPY strongswanKey.pem /etc/swanctl/x509ca/strongswanKey.pem

# we generate the private key
RUN pki --gen --type ed25519 --outform pem > /etc/swanctl/private/sunKey.pem 

# next, we create a certificate request that has to be signed by the CA
RUN pki --req --type priv --in /etc/swanctl/private/sunKey.pem \
          --dn "C=CH, O=strongswan, CN=sun.strongswan.org" \
          --san sun.strongswan.org --outform pem > /etc/swanctl/private/sunReq.pem

# CA signs the end entity generating the certificate
RUN pki --issue --cacert /etc/swanctl/x509ca/strongswanCert.pem --cakey /etc/swanctl/x509ca/strongswanKey.pem \
            --type pkcs10 --in /etc/swanctl/private/sunReq.pem --serial 01 --lifetime 1826 \
            --outform pem > /etc/swanctl/x509/sunCert.pem
# List the issued host certificate
RUN pki --print --in /etc/swanctl/x509/sunCert.pem

# Expose ports
EXPOSE 500/udp 4500/udp 50/udp 51/udp 179 2601 2605

# Entrypoint
ENTRYPOINT ["/etc/ENTRYPOINT.sh"]
