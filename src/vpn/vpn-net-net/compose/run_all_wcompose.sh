#!/bin/bash
docker-compose up -d
docker exec -it h1_moon ip route del default
docker exec -it h1_moon ip route add default via 10.1.0.254
docker exec -it h1_sun ip route del default
docker exec -it h1_sun ip route add default via 10.2.0.254
