#!/bin/bash
docker-compose up -d

# MOON
docker exec -it moon ip route del default
docker exec -it moon ip route add default via 137.204.0.2

# NAT
docker exec -it nat ip route del default
docker exec -it nat ip route add default via 137.204.0.3

# NAT rules
# simple 1:1 map: forward all eth0 incoming packets to 192.168.1.10
docker exec -it nat iptables -t nat -A PREROUTING -i eth0 -j DNAT --to 192.168.1.10
# masquerade private LAN packets leaving eth1 with its (public) IP
docker exec -it nat iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE

# HOST1_MOON
docker exec -it h1_moon ip route del default
docker exec -it h1_moon ip route add default via 10.1.0.254

# CAROL ROADWARRIOR
docker exec -it carol ip route del default
docker exec -it carol ip route add default via 192.168.1.254
