#!/bin/bash
# image build
echo "----------- DOCKER IMAGES ----------"
docker build -t moon-rw ./images/moon/
docker build -t carol ./images/carol/
docker build -t nat ./images/nat/

# network behind moon VPN gateway
echo "------------- NETWORKS -------------"
docker network create --subnet=10.1.0.0/24 -o com.docker.network.bridge.name=br_moon_lan moon_lan
# network between NAT and VPN gateway
docker network create --subnet=137.204.0.0/24 -o com.docker.network.bridge.name=br_lan lan
# private network behind NAT
docker network create --subnet=192.168.1.0/24 -o com.docker.network.bridge.name=br_pr_lan pr_lan

# moon VPN gateway container run
echo "--------- MOON VPN GATEWAY ---------"
docker run -d -it --privileged \
	--name moon \
	-v $PWD/conf/moon/swanctl.conf:/etc/swanctl/swanctl.conf \
	--network moon_lan --ip 10.1.0.254 \
	moon-rw
docker network connect --ip 137.204.0.3 lan moon

# change default route
docker exec -it moon ip route del default
docker exec -it moon ip route add default via 137.204.0.2

# NAT
echo "--------------- NAT ----------------"
docker run -d -it --privileged \
	--name nat \
	--network pr_lan --ip 192.168.1.254 \
	nat

docker network connect --ip 137.204.0.2 lan nat
docker exec -it nat ip route del default
docker exec -it nat ip route add default via 137.204.0.3

# NAT rules
# simple 1:1 map: forward all eth1 incoming packets to 192.168.1.10
docker exec -it nat iptables -t nat -A PREROUTING -i eth1 -j DNAT --to 192.168.1.10
# masquerade private LAN packets leaving eth1 with its (public) IP
docker exec -it nat iptables -t nat -A POSTROUTING -o eth1 -j MASQUERADE

# host behind VPN gateway
echo "----------- HOST1_MOON -------------"
docker run -d -it --privileged --name h1_moon --network moon_lan --ip 10.1.0.10 alpine
docker exec -it h1_moon ip route del default
docker exec -it h1_moon ip route add default via 10.1.0.254

# carol: roadwarrior
echo "-------- CAROL ROADWARRIOR ---------"
docker run -d -it --privileged \
	--name carol \
	--network pr_lan --ip 192.168.1.10 \
	-v $PWD/conf/carol/swanctl.conf:/etc/swanctl/swanctl.conf \
	carol
docker exec -it carol ip route del default
docker exec -it carol ip route add default via 192.168.1.254

# test: ping a host behind VPN gateway
echo "------- CAROL --> HOST1_SUN --------"
docker exec -it carol ping 10.1.0.10
